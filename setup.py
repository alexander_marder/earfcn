import sys
from setuptools import setup, find_packages

# read the contents of your README file
from pathlib import Path
this_directory = Path(__file__).parent
long_description = (this_directory / "README.md").read_text()

setup(
    name="earfcn",
    version='0.0.3',
    author='Alex Marder',
    # author_email='notlisted',
    description="Convert LTE EARFCN to frequency, or frequency to EARFCN.",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/alexander_marder/earfcn",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'earfcn2freq=earfcn.convert:earfcn2freq_main',
            'freq2earfcn=earfcn.convert:freq2earfcn_main',
        ],
    },
    # install_requires=['numpy'],
    zip_safe=False,
    include_package_data=True,
    python_requires='>3.6'
)
